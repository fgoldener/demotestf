package com.testeoF.repo;




import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.testeoF.module.Mensaje;

@Repository
public interface MensajeRepository extends JpaRepository<Mensaje, Long>{
	

}
